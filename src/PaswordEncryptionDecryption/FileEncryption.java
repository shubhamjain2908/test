/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PaswordEncryptionDecryption;

/**
 *
 * @author SHUBHAM
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class FileEncryption
{
    ArrayList<String> al=new ArrayList<>();
    public ArrayList<String> bufferFile()
    {
        FileReader fr=null;
        BufferedReader br=null;
        try 
        {
            fr = new FileReader("C:/SJP/File/BRRead.java");
            br= new BufferedReader(fr);
            String str;
            do
            {
                str = br.readLine();
                if(str!=null)
                {
                    System.out.println(str);
                    al.add(str);
                }
            }
            while(str!=null);
            fr.close();
        } 
        catch (FileNotFoundException ex) 
        {
            Logger.getLogger(FileEncryption.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex) 
        {
            Logger.getLogger(FileEncryption.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally 
        {
            return al;
        }
    }
    
    public void saveEcFile(ArrayList<String> ec)
    {
        String str;
        FileWriter fw;
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
        try
        {
            fw=new FileWriter("C:/SJP/file/FileWrite.txt");
            for (int i = 0; i < ec.size(); i++) 
            {
                fw.write(ec.get(i));
            }
            fw.close();
        }
        catch(IOException e)
        {
            System.out.println("can not open file");
            return;
        }
    }
    
    public void displayDcFile()
    {
        try
        {
            FileReader fr=new FileReader("C:/SJP/File/FileWrite.txt");
            BufferedReader br= new BufferedReader(fr);
            String str="";
            KeyGenerator keygenerator = KeyGenerator.getInstance("DES");
            SecretKey myDesKey = keygenerator.generateKey();

            Cipher desCipher;
            desCipher = Cipher.getInstance("DES");

            do
            {
                str = br.readLine();
                if(str!=null)
                {
                    try
                    {
                        String s;
                        desCipher.init(Cipher.DECRYPT_MODE, myDesKey);
                        byte[] textDecrypted = desCipher.doFinal(str.getBytes());

                        s = new String(textDecrypted);
                        System.out.println("Decrypted : "+s);
                    }
                    catch(Exception e)
                    {
                        System.out.println(e);
                    }
                }
            }
            while(str!=null);
            fr.close();
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
    }
    
    public void dcFile(ArrayList<String> dc)
    {
        for (int i = 0; i < dc.size(); i++) 
        {
            System.out.println(dc.get(i));
        }
    }
    
    public static void main(String[] args) 
    {
        FileEncryption fe=new FileEncryption();
        ArrayList<String> br1=fe.bufferFile();
        ArrayList<String> ec=new ArrayList<>();
        ArrayList<String> dc=new ArrayList<>();
        try
        {
            KeyGenerator keygenerator = KeyGenerator.getInstance("DES");
            SecretKey myDesKey = keygenerator.generateKey();

            Cipher desCipher;
            desCipher = Cipher.getInstance("DES");

            for (int i = 0; i < br1.size(); i++) 
            {
                byte[] text = br1.get(i).getBytes("UTF8");

                desCipher.init(Cipher.ENCRYPT_MODE, myDesKey);
                byte[] textEncrypted = desCipher.doFinal(text);

                String s = new String(textEncrypted);
                //System.out.println("Encrypted : "+s);
                ec.add(s);

                desCipher.init(Cipher.DECRYPT_MODE, myDesKey);
                byte[] textDecrypted = desCipher.doFinal(textEncrypted);

                s = new String(textDecrypted);
                //System.out.println("Decrypted : "+s);
                dc.add(s);
            }
            //fe.saveEcFile(ec);
            fe.displayDcFile();
            //fe.dcFile(dc);
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }
}