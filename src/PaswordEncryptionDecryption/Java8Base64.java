/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PaswordEncryptionDecryption;

/**
 *
 * @author SHUBHAM
 */
import java.util.Base64;
import java.util.UUID;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Java8Base64 
{
    public void ED()
    {
        String sj="Shubham";
        String encoded;
        try 
        {
            encoded = Base64.getEncoder().encodeToString(sj.getBytes("utf-8"));
            System.out.println(encoded);
            byte[] decode=Base64.getDecoder().decode(encoded);
            String ss=new String(decode,"utf-8");
            System.out.println(ss);
        } 
        catch (UnsupportedEncodingException ex) 
        {
            Logger.getLogger(Java8Base64.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void Uri()
    {
        String u="google.com";
        try 
        {
            String urlEncode= Base64.getUrlEncoder().encodeToString(u.getBytes("utf-8"));
            System.out.println(urlEncode);
            byte[] d=Base64.getUrlDecoder().decode(u);
            String dec=new String(d,"utf-8");
            System.out.println(dec);
        } 
        catch (UnsupportedEncodingException ex) 
        {
            Logger.getLogger(Java8Base64.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void mime()
    {
        String m="Shubham";
        try 
        {
            StringBuilder stringBuilder = new StringBuilder();
		
            for (int i = 0; i < 10; ++i) 
            {
                stringBuilder.append(UUID.randomUUID().toString());
                //System.out.println(stringBuilder);
            }
            System.out.println("String builder :     "+stringBuilder.toString());
            String mimeE=Base64.getMimeEncoder().encodeToString(stringBuilder.toString().getBytes("utf-8"));
            System.out.println("Encoded :     "+mimeE);
            byte[] mi=Base64.getMimeDecoder().decode(mimeE);
            String decodeM=new String(mi,"utf-8");
            System.out.println("DEcoded   :   "+decodeM);
        }
        catch (UnsupportedEncodingException ex) 
        {
            Logger.getLogger(Java8Base64.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void sB()
    {
        StringBuilder stringBuilder = new StringBuilder();
		
        for (int i = 0; i < 10; ++i) 
        {
            stringBuilder.append(UUID.randomUUID().toString());
            System.out.println(stringBuilder);
        }
    }
   public static void main(String args[]) 
   {
    Java8Base64 j=new Java8Base64();
    //j.ED();
    //j.Uri();
    j.mime();
    //j.sB();
    int jj=0;
    if(jj>0)
    {
    
      try
      {
		
         // Encode using basic encoder
         String base64encodedString = Base64.getEncoder().encodeToString(
            "TutorialsPoint?java8".getBytes("utf-8"));
         System.out.println("Base64 Encoded String (Basic) :" + base64encodedString);
		
         // Decode
         byte[] base64decodedBytes = Base64.getDecoder().decode(base64encodedString);
		
         System.out.println("Original String: " + new String(base64decodedBytes, "utf-8"));
        
         //Url encoding
         base64encodedString = Base64.getUrlEncoder().encodeToString(
            "TutorialsPoint?java8".getBytes("utf-8"));
         System.out.println("Base64 Encoded String (URL) :" + base64encodedString);
		
        StringBuilder stringBuilder = new StringBuilder();
		
        for (int i = 0; i < 10; ++i) 
        {
            stringBuilder.append(UUID.randomUUID().toString());
            System.out.println(stringBuilder);
        }
        //stringBuilder.append("TutorialsPoint?java8");
	//MIME	
         byte[] mimeBytes = stringBuilder.toString().getBytes("utf-8");
         String mimeEncodedString = Base64.getMimeEncoder().encodeToString(mimeBytes);
         System.out.println("Base64 Encoded String (MIME) :" + mimeEncodedString);

      }
      catch(UnsupportedEncodingException e) 
      {
         System.out.println("Error :" + e.getMessage());
      }
    } 
   }
}