/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PaswordEncryptionDecryption;

/**
 *
 * @author SHUBHAM
 */
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Random;

public class BlowFishAlgorithm 
{
    byte[] skey = new byte[1000];
    String skeyString;
    static byte[] raw;
    String inputMessage, encryptedData, decryptedMessage;

    public BlowFishAlgorithm() 
    {
        try 
        {
            generateSymmetricKey();

            inputMessage = "Shubham Jain";
            byte[] ibyte = inputMessage.getBytes();
            byte[] ebyte = encrypt(raw, ibyte);
            String encryptedData = new String(ebyte);
            System.out.println("Encrypted message " + encryptedData);
            //JOptionPane.showMessageDialog(null, "Encrypted Data " + "\n" + encryptedData);

            byte[] dbyte = decrypt(raw, ebyte);
            String decryptedMessage = new String(dbyte);
            System.out.println("Decrypted message " + decryptedMessage);

            //JOptionPane.showMessageDialog(null, "Decrypted Data " + "\n" + decryptedMessage);
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }

    void generateSymmetricKey() 
    {
        try 
        {
            Random r = new Random();
            int num = r.nextInt(10000);
            String knum = String.valueOf(num);
            byte[] knumb = knum.getBytes();
            skey = getRawKey(knumb);
            skeyString = new String(skey);
            System.out.println("Blowfish Symmetric key = " + skeyString);
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
    }

    private static byte[] getRawKey(byte[] seed) throws Exception 
    {
        KeyGenerator kgen = KeyGenerator.getInstance("Blowfish");
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        sr.setSeed(seed);
        kgen.init(128, sr); // 128, 256 and 448 bits may not be available
        SecretKey skey = kgen.generateKey();
        raw = skey.getEncoded();
        return raw;
    }

    private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception 
    {System.out.println("Raw Encrypted: " + raw);
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "Blowfish");
        Cipher cipher = Cipher.getInstance("Blowfish");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(clear);
        System.out.println("Encrypted Byte: "+encrypted);
        return encrypted;
    }

    private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception 
    {System.out.println("Raw Decrypted: " + raw);
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "Blowfish");
        Cipher cipher = Cipher.getInstance("Blowfish");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }

    public static void main(String args[]) 
    {
        BlowFishAlgorithm bf = new BlowFishAlgorithm();
    }
}
	
//import javax.crypto.Cipher;
//import javax.crypto.spec.SecretKeySpec;
//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;
//public class BlowFishAlgorithm {
// 
//    public static void main(String[] args) throws Exception {
//        encrypt("edwin","password");
//        decrypt("6VsVtA/nhHKUZuWWmod/BQ==");
//    }
// 
//    private static void encrypt(String username, String password) throws Exception {
//        byte[] keyData = (username+password).getBytes();
//        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, "Blowfish");
//        Cipher cipher = Cipher.getInstance("Blowfish");
//        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
//        byte[] hasil = cipher.doFinal(password.getBytes());
//        System.out.println(new BASE64Encoder().encode(hasil));
//    }
//     
//    private static void decrypt(String string) throws Exception {
//        byte[] keyData = ("edwin"+"password").getBytes();
//        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, "Blowfish");
//        Cipher cipher = Cipher.getInstance("Blowfish");
//        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
//        byte[] hasil = cipher.doFinal(new BASE64Decoder().decodeBuffer(string));
//        System.out.println(new String(hasil));
//    }
//}
