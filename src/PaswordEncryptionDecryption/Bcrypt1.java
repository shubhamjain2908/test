/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PaswordEncryptionDecryption;

import java.security.NoSuchAlgorithmException;

/**
 *
 * @author SHUBHAM
 */
public class Bcrypt1
{
    public static void main(String[] args) throws NoSuchAlgorithmException
    {
        String  originalPassword = "password";
        String ppp="password";
        String generatedSecuredPasswordHash = BCrypt.hashpw(originalPassword, BCrypt.gensalt(12));
        System.out.println(generatedSecuredPasswordHash);
         
        boolean matched = BCrypt.checkpw(ppp, generatedSecuredPasswordHash);
        System.out.println(matched);
    }
}