/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileReadingAndGeneratingAnalytics;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author SHUBHAM
 */
public class countLines 
{
    public static void main(String[] args) throws IOException 
    {
        countLines c=new countLines();
        int cc=c.count("C:/logs/spring.log");
        System.out.println(cc);
        int c1=c.count("C:/logs/spring1.log");
        System.out.println(c1);
    }
    public int count(String filename) throws IOException 
    {
        InputStream is = new BufferedInputStream(new FileInputStream(filename));
        try 
        {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) 
            {
                empty = false;
                for (int i = 0; i < readChars; ++i) 
                {
                    if (c[i] == '\n') 
                    {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count;
        }
        finally 
        {
            is.close();
        }
    }
}
