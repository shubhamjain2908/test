/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileReadingAndGeneratingAnalytics;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;

/**
 *
 * @author SHUBHAM
 */
public class DeleteFile 
{
    public void asdsd(String[] args)
    {
        try
        {
            Files.deleteIfExists(Paths.get("C:\\QuizReport\\reportIT.xlsx"));
        }
        catch(NoSuchFileException e)
        {
            System.out.println("No such file/directory exists");
        }
        catch(DirectoryNotEmptyException e)
        {
            System.out.println("Directory is not empty.");
        }
        catch(IOException e)
        {
            System.out.println("Invalid permissions.");
        }
         
        System.out.println("Deletion successful.");
    }
    
    public static void main(String[] args)
    {
        File file = new File("C:\\QuizReport\\report0812it151053.xlsx");
         
        if(file.delete())
        {
            System.out.println("File deleted successfully");
        }
        else
        {
            System.out.println("Failed to delete the file");
        }
    }
}
