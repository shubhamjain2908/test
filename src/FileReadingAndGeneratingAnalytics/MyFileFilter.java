/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileReadingAndGeneratingAnalytics;

import java.io.File;
import java.io.FilenameFilter;

/**
 *
 * @author SHUBHAM
 */
public class MyFileFilter {
 
    public static void main(String a[]){
        File file = new File("C:\\logs\\spring.log");
        MyFileFilter m=new MyFileFilter();
        String n=file.getName();
        System.out.println(file+" : "+n);
        String[] files = file.list(new FilenameFilter() {
             
            @Override
            public boolean accept(File file, String n) {
                System.out.println(file+"  : "+n);
                if(n.toLowerCase().contains("DEBUG")){
                    return true;
                } else {
                    return false;
                }
            }
        });
        
        for(String f:files){
          System.out.println(f);
        }
    }
}