/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileReadingAndGeneratingAnalytics;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 *
 * @author SHUBHAM
 */
public class FileReadingUsingStream 
{
    public static void main(String[] args) throws IOException 
    {
        Path path = Paths.get("C:\\logs\\spring.log");
        System.out.println("path : "+path);
        Stream<String> streamOfStrings = Files.lines(path);
        Stream<String> streamWithCharset = 
        Files.lines(path, Charset.forName("UTF-8"));
        streamWithCharset.forEach(p-> System.out.println(p));
    }
}
