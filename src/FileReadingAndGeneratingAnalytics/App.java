/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileReadingAndGeneratingAnalytics;

/**
 *
 * @author SHUBHAM
 */
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
 
public class App 
{
    public static void main(String[] argsas) throws IOException 
    {
        Stream<String> lines = Files.lines(Paths.get("C:/logs/spring.log"));
        new App().parse(lines, "C:/logs/spring1.log");
    }
     
    private void parse(Stream<String> lines, String output) throws IOException 
    {
        final FileWriter fw = new FileWriter(output);
         
        //@formatter:off
        lines.filter(line -> line.contains("DEBUG"))
                .map(line -> line.split("                          "))
                    .map(arr -> arr[arr.length - 1])
                        .forEach(packageT -> writeToFile(fw, packageT));
        //@formatter:on
        fw.close();
        lines.close();
    }
 
    private void writeToFile(FileWriter fw, String packageT) 
    {
        try 
        {
            fw.write(String.format("%s%n", packageT));
        }
        catch (IOException e) 
        {
            throw new RuntimeException(e);
        }
    }
 
}