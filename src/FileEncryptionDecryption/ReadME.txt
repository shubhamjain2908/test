
AESFileE and D :-

In this tutorial we will have simple text file with plain text.
This can be any type of file. We will encrypt this into another file with an extension .des For encryption we will use a plain text password, then an iv which is used to initialize the cipher and a salt which is used for encoding.

All these three, password, iv and salt should be passed on to the recipient in a secure mechanism and not along with the encrypted file. The recipient will use the plain text password, iv (to initialize the cypher) and the salt to decrypt the file. In our example tutorial, we will again write back the decrypted content to a plain text file. So once the process is over the input text file and the output text file content should match.

CryptoTest : -

original file : file.txt
encrypted file : en.encrypted
decrypted file : dc.txt