/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileEncryptionDecryption;

/**
 *
 * @author SHUBHAM
 */
import java.io.File;
 
/**
 * A tester for the CryptoUtils class.
 * @author www.codejava.net
 *
 */
public class CryptoUtilsTest 
{
    public static void main(String[] args) 
    {
        String key = "Mary has one cat";
        File inputFile = new File("C:/SJP/TestFileEDC/document.txt");
        File encryptedFile = new File("C:/SJP/TestFileEDC/document.encrypted");
        File decryptedFile = new File("C:/SJP/TestFileEDC/document.decrypted");
         
        try 
        {
            CryptoUtils.encrypt(key, inputFile, encryptedFile);
            CryptoUtils.decrypt(key, encryptedFile, decryptedFile);
        } 
        catch (CryptoException ex) 
        {
            System.out.println(ex.getMessage());
        }
    }
}