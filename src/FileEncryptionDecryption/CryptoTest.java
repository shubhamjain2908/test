/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileEncryptionDecryption;

/**
 *
 * @author SHUBHAM
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class CryptoTest 
{
    private static final String fileToBeEncrypted="C:\\zip\\API_Document.docx";
    private static final String EncryptedFile="C:\\zip\\e\\API_Document.docx";
    private static final String DecryptedFile="C:\\zip\\d\\API_Document.docx";
    
    static void fileProcessor(int cipherMode,String key,File inputFile,File outputFile)
    {
        try (FileOutputStream outputStream = new FileOutputStream(outputFile);FileInputStream inputStream = new FileInputStream(inputFile);)
        {
            Key secretKey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(cipherMode, secretKey);

            //FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);

            byte[] outputBytes = cipher.doFinal(inputBytes);

            //FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);

            //inputStream.close();
            //outputStream.close();

        }
        catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | IOException e) 
        {
            System.out.println(e);
        }
    }
	
    public static void main(String[] args) 
    {
	String key = "1234567890123456";
        String key1= "1234567890123456";
        
	File inputFile = new File(fileToBeEncrypted);
	File encryptedFile = new File(EncryptedFile);
	File decryptedFile = new File(DecryptedFile);
		
	try 
        {
            CryptoTest.fileProcessor(Cipher.ENCRYPT_MODE,key,inputFile,encryptedFile);
	    CryptoTest.fileProcessor(Cipher.DECRYPT_MODE,key1,encryptedFile,decryptedFile);
            System.out.println("Sucess");
	}
        catch (Exception ex) 
        {
            System.out.println(ex.getMessage());
        }
    }
	
}