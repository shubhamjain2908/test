/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileCompression;

/**
 *
 * @author SHUBHAM
 */
import java.io.*;
import java.util.zip.*;

public class ReadEmployee8 
{
    public static void main(String argv[]) throws Exception 
    {
        //deserialize objects sarah and sam
        FileInputStream fis = new FileInputStream("c:\\zip\\se7.zip");
        GZIPInputStream gs = new GZIPInputStream(fis);
        ObjectInputStream ois = new ObjectInputStream(gs);
        Employee6 sarah = (Employee6) ois.readObject();
        Employee6 sam = (Employee6) ois.readObject();
        //print the records after reconstruction of state
        sarah.print();
        sam.print();
        ois.close();
        fis.close();
    }
}
