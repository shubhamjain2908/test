/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileCompression;

/**
 *
 * @author SHUBHAM
 */
import java.io.*;
import java.util.zip.*;

public class UnZip5 
{
    static final String storedPath="C:\\zip\\";
    public static void main(String argv[]) 
    {
        try 
        {
            final int BUFFER = 2048;
            BufferedOutputStream dest = null;
            FileInputStream fis = new FileInputStream("C:\\zip\\myfigs.zip");
            CheckedInputStream checksum = new CheckedInputStream(fis, new Adler32());
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(checksum));
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) 
            {
                System.out.println("Extracting: " + entry);
                int count;
                byte data[] = new byte[BUFFER];
                // write the files to the disk
                FileOutputStream fos = new FileOutputStream(storedPath+entry.getName());
                dest = new BufferedOutputStream(fos,BUFFER);
                while ((count = zis.read(data, 0,BUFFER)) != -1) 
                {
                    dest.write(data, 0, count);
                }
                dest.flush();
                dest.close();
            }
            zis.close();
            System.out.println("Checksum:"+checksum.getChecksum().getValue());
        } 
        catch (IOException e) 
        {
            System.out.println(e);
        }
    }
}
