/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileCompression;

/**
 *
 * @author SHUBHAM
 */
import java.io.*;
import java.util.*;
import java.util.zip.*;

public class UnZip2 
{
   static final int BUFFER = 2048;
   static final String storedPath="C:\\zip\\";
   public static void main (String argv[]) 
   {
        try 
        {
            BufferedOutputStream dest = null;
            BufferedInputStream is = null;
            ZipEntry entry;
            ZipFile zipfile = new ZipFile("C:\\zip\\myfigs.zip");
            Enumeration e = zipfile.entries();
            while(e.hasMoreElements()) 
            {
                entry = (ZipEntry) e.nextElement();
                System.out.println("Extracting: " +entry);
                is = new BufferedInputStream(zipfile.getInputStream(entry));
                int count;
                byte data[] = new byte[BUFFER];
                FileOutputStream fos = new FileOutputStream(storedPath+entry.getName());
                dest = new BufferedOutputStream(fos, BUFFER);
                while ((count = is.read(data, 0, BUFFER))!= -1) 
                {
                   dest.write(data, 0, count);
                }
                dest.flush();
                dest.close();
                is.close();
            }
        }
        catch(IOException e) 
        {
            System.out.println(e);
        }
   }
}


