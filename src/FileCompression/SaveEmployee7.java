/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileCompression;

/**
 *
 * @author SHUBHAM
 */
import java.io.*;
import java.util.zip.*;

public class SaveEmployee7 
{
    public static void main(String argv[]) throws 
    Exception 
    {
        // create some objects
        Employee6 sarah = new Employee6("S. Jordan", 28,56000);
        Employee6 sam = new Employee6("S. McDonald", 29,58000);
        // serialize the objects sarah and sam
        FileOutputStream fos = new FileOutputStream("c:\\zip\\se7.zip");
        GZIPOutputStream gz = new GZIPOutputStream(fos);
        ObjectOutputStream oos = new ObjectOutputStream(gz);
        oos.writeObject(sarah);
        oos.writeObject(sam);
        oos.flush();
        oos.close();
        fos.close();
    }
} 