package FileCompression;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author SHUBHAM
 */
import java.io.*;
import java.util.zip.*;

public class Zip4 
{

    static final int BUFFER = 2048;

    public static void main(String argv[]) 
    {
        try 
        {
            String path="C:\\SJP\\file";
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream("c:\\zip\\myfigs.zip");
            CheckedOutputStream checksum = new CheckedOutputStream(dest, new Adler32());
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(checksum));
            //out.setMethod(ZipOutputStream.DEFLATED);
            byte data[] = new byte[BUFFER];
            // get a list of files from current directory
            File f = new File(path);
            String files[] = f.list();

            for (int i = 0; i < files.length; i++) 
            {
                System.out.println("Adding: " + files[i]);
                FileInputStream fi = new FileInputStream(path+"\\"+files[i]);
                origin = new BufferedInputStream(fi, BUFFER);
                ZipEntry entry = new ZipEntry(files[i]);
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0,BUFFER)) != -1) 
                {
                    out.write(data, 0, count);
                }
                origin.close();
            }
            out.close();
            System.out.println("checksum:" + checksum.getChecksum().getValue());
        } 
        catch (IOException e) 
        {
            System.out.println(e);
        }
    }
}
