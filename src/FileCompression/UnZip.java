/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileCompression;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 *
 * @author SHUBHAM
 */

public class UnZip 
{
   final static int BUFFER = 2048;
   static final String storedPath="C:\\zip\\";
   public static void main (String argv[]) 
   {
       //int BUFFER = 2048;
        try 
        {
            BufferedOutputStream dest = null;
            FileInputStream fis = new FileInputStream("C:/SJP/TestFile.zip");
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
            ZipEntry entry;
            while((entry = zis.getNextEntry()) != null) 
            {
                System.out.println("Extracting: " +entry);
                int count;
                byte data[] = new byte[BUFFER];
                // write the files to the disk
                FileOutputStream fos = new FileOutputStream(storedPath+entry.getName());
                dest = new BufferedOutputStream(fos, BUFFER);
                while ((count = zis.read(data, 0, BUFFER))!= -1) 
                {
                   dest.write(data, 0, count);
                }
                dest.flush();
                dest.close();
            }
            zis.close();
        }
        catch(IOException e) 
        {
            System.out.println(e);
        }
    }
}
