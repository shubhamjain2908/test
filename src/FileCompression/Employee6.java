/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileCompression;

/**
 *
 * @author SHUBHAM
 */
import java.io.*;

public class Employee6 implements Serializable 
{
   String name;
   int age;
   int salary;

   public Employee6(String name, int age, int salary) 
   {
      this.name = name;
      this.age = age;
      this.salary = salary;
   }

   public void print() 
   {
      System.out.println("Record for: "+name);
      System.out.println("Name: "+name);
      System.out.println("Age: "+age);
      System.out.println("Salary: "+salary);
  }
}
 